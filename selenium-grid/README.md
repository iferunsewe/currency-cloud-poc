﻿Hub
---
java -jar selenium-server-standalone-2.41.0.jar -role hub -port 4444

Node
----
java -jar selenium-server-standalone-2.41.0.jar -role node -port 5556 -hub http://localhost:4444/grid/register -browser browserName=chrome,maxInstances=5,platform=WINDOWS -Dwebdriver.chrome.driver=..\bin\chromedriver\chromedriver.exe

java -jar selenium-server-standalone-2.41.0.jar -role node -port 5556 -hub http://localhost:4444/grid/register -browser browserName=firefox,maxInstances=2,platform=MAC

Console
-------
http://localhost:4444/grid/console