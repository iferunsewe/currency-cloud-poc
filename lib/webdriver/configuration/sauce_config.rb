module WebDriver
  module Configuration
    class SauceConfig < Base
      def initialize(environment)
        super
      end

      def remote_url
        "http://" + sauce_credentials + "@ondemand.saucelabs.com:80/wd/hub"
      end

      def sauce_user
        raise(EnvironmentError, "Could not find property with key: '#{Keys::Sauce::SAUCE_USERNAME}' in the env.yml file.\n") unless @environment.has_property?(Keys::Sauce::SAUCE_USERNAME)
        @environment.property(Keys::Sauce::SAUCE_USERNAME)
      end

      def sauce_access_key
        raise(EnvironmentError, "Could not find property with key: '#{Keys::Sauce::SAUCE_ACCESS_KEY}' in the env.yml file.\n") unless @environment.has_property?(Keys::Sauce::SAUCE_ACCESS_KEY)
        @environment.property(Keys::Sauce::SAUCE_ACCESS_KEY)
      end

      def chrome_capabilities
        capabilities = Selenium::WebDriver::Remote::Capabilities.chrome("chromeOptions" => { "args" => chrome_switches })
        capabilities[:platform] = platform
        capabilities[:version] = browser_version
        capabilities["acceptSslCerts"] = true
        # Issue: https://code.google.com/p/selenium/issues/detail?id=4663
        # Solution: http://support.saucelabs.com/entries/24863243-update-chromedriver-to-2-1
        capabilities["chromedriver-version"] = chrome_driver_version
        capabilities["selenium-version"] = selenium_version
        capabilities
      end

      def firefox_capabilities
        capabilities = Selenium::WebDriver::Remote::Capabilities.firefox
        capabilities[:firefox_profile] = firefox_profile
        capabilities[:platform] = platform
        capabilities[:version] = browser_version
        capabilities
      end

      def ie_capabilities
        capabilities = Selenium::WebDriver::Remote::Capabilities.internet_explorer
        capabilities[:platform] = platform
        capabilities[:version] = browser_version
        capabilities["acceptSslCerts"] = true
        capabilities
      end

      def safari_capabilities
        capabilities = Selenium::WebDriver::Remote::Capabilities.safari
        capabilities[:platform] = platform
        capabilities[:version] = browser_version
        capabilities["acceptSslCerts"] = true
        capabilities
      end

      private

      def sauce_credentials
        sauce_user + ":" + sauce_access_key
      end

      def chrome_driver_version
        raise(EnvironmentError, "Could not find property with key: 'chromedriver-version' in the env.yml file.\n") unless @environment.has_property?("chromedriver-version")
        @environment.property("chromedriver-version")
      end

      def selenium_version
        raise(EnvironmentError, "Could not find property with key: 'selenium-version' in the env.yml file.\n") unless @environment.has_property?("selenium-version")
        @environment.property("selenium-version")
      end

      def platform
        raise(EnvironmentError, "Could not find property with key: '#{Keys::Selenium::PLATFORM}' in the env.yml file.\n") unless @environment.has_property?(Keys::Selenium::PLATFORM)
        platform = @environment.property(Keys::Selenium::PLATFORM)
        raise(UnsupportedConfigurationError, "The platform: #{platform} is not currently supported by Sauce Labs.\n") unless platform_valid?(platform)
        platform
      end

      def platform_valid?(platform)
        platform.eql?("OS X 10.9") ||
            platform.eql?("OS X 10.8") ||
            platform.eql?("OS X 10.6") ||
            platform.eql?("Linux") ||
            platform.eql?("Windows 8.1") ||
            platform.eql?("Windows 8") ||
            platform.eql?("Windows 7") ||
            platform.eql?("Windows XP")
      end
    end
  end
end
