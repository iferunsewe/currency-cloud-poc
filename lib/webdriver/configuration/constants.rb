module WebDriver
  module Configuration
    class Constants
      DEFAULT_GRID_HUB_URL = "http://localhost:4444/wd/hub"
      DEFAULT_APPIUM_HUB_URL = "http://127.0.0.1:4723/wd/hub"

      class BrowserName
        CHROME = "chrome"
        FIREFOX = "firefox"
        IE = "ie"
        SAFARI = "safari"
        PHANTOMJS = "phantomjs"
      end

      class Device
        IPHONE = "ipad"
        IPAD = "iphone"
        ANDROID = "android"
        SELENDROID = "selendroid"
      end
    end
  end
end