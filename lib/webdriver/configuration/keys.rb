module WebDriver
  module Configuration
    class Keys
      LOCATOR_PROFILE = "locator-profile"

      class Selenium
        GRID_HUB_URL = "grid-hub-url"

        PLATFORM = "platform"
        BROWSER_NAME = "browser-name"
        BROWSER_VERSION = "browser-version"
        BROWSER_SIZE = "browser-size"
        DEVICE_ORIENTATION = "device-orientation"
        DEVICE_TYPE = "device-type"

        CHROME_DRIVER_PATH = "chrome-driver-path"
        CHROME_BINARY = "chrome-binary"
        CHROME_SWITCHES = "chrome-switches"

        FIREFOX_BINARY = "firefox-binary"
        FIREFOX_PREFERENCES = "firefox-preferences"

        IE_DRIVER_PATH = "ie-driver-path"
      end

      class Appium
        APPIUM_HUB_URL = "appium-hub-url"

        APP = "app"
        BROWSER_NAME = "browser-name"
        DEVICE = "device"
        VERSION = "version"
        NEW_COMMAND_TIMEOUT = "new-command-timeout"
        LAUNCH = "launch"

        # Android only
        APP_ACTIVITY = "app-activity"
        APP_PACKAGE = "app-package"
        APP_WAIT_ACTIVITY = "app-wait-activity"
        DEVICE_READY_TIMEOUT = "device-ready-timeout"
        COMPRESS_XML = "compress-xml"

        # iOS only
        CALENDAR_FORMAT = "calendar-format"
        DEVICE_NAME = "device-name"
        LANGUAGE = "language"
        LAUNCH_TIMEOUT = "launch-timeout"
        LOCALE = "locale"
        LOCATION_SERVICES_ENABLED = "location-services-enabled"
        AUTO_ACCEPT_ALERTS = "auto-accept-alerts"
        NATIVE_INSTRUMENTS_LIB = "native-instruments-lib"
      end

      class Sauce
        SAUCE_USERNAME = "sauce-username"
        SAUCE_ACCESS_KEY = "sauce-access-key"
      end
    end
  end
end