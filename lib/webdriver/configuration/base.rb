module WebDriver
  module Configuration
    class Base
      def initialize(environment)
        @environment = environment
      end

      def has_device?
        @environment.has_property?(Keys::Appium::DEVICE)
      end

      def browser_name
        @environment.property(Keys::Selenium::BROWSER_NAME)
      end

      def browser_version
        return @environment.property(Keys::Selenium::BROWSER_VERSION) if @environment.has_property?(Keys::Selenium::BROWSER_VERSION)
        ""
      end

      def chrome_switches
        return @environment.property(Keys::Selenium::CHROME_SWITCHES) if @environment.has_property?(Keys::Selenium::CHROME_SWITCHES)
        []
      end

      def firefox_profile
        profile = Selenium::WebDriver::Firefox::Profile.new
        profile.native_events = false
        profile.secure_ssl = true
        profile.assume_untrusted_certificate_issuer = true
        profile
      end

      def device
        raise(EnvironmentError, "Could not find property with key: '#{Keys::Appium::DEVICE}' in the env.yml file.") unless @environment.has_property?(Keys::Appium::DEVICE)
        @environment.property(Keys::Appium::DEVICE)
      end

      def device_orientation
        return "landscape" unless @environment.has_property?(Keys::Selenium::DEVICE_ORIENTATION)
        device_orientation = @environment.property(Keys::Selenium::DEVICE_ORIENTATION)
        raise(EnvironmentError, "The device orientation must be either portrait or landscape.\n") unless device_orientation.eql?("landscape") || device_orientation.eql?("portrait")
        device_orientation
      end

      def device_type
        return "phone" unless @environment.has_property?(Keys::Selenium::DEVICE_TYPE)
        device_type = @environment.property(Keys::Selenium::DEVICE_TYPE)
        raise(EnvironmentError, "The device type must be either phone or tablet.\n") unless device_type.eql?("phone") || device_type.eql?("tablet")
        device_type
      end

      def android_capabilities
        capabilities = Selenium::WebDriver::Remote::Capabilities.new
        server_capabilities(capabilities)
        capabilities[AppiumCapabilityType::Android::APP_ACTIVITY] = @environment.property(Keys::Appium::APP_ACTIVITY) if @environment.has_property?(Keys::Appium::APP_ACTIVITY)
        capabilities[AppiumCapabilityType::Android::APP_PACKAGE] = @environment.property(Keys::Appium::APP_PACKAGE) if @environment.has_property?(Keys::Appium::APP_PACKAGE)
        capabilities[AppiumCapabilityType::Android::APP_WAIT_ACTIVITY] = @environment.property(Keys::Appium::APP_WAIT_ACTIVITY) if @environment.has_property?(Keys::Appium::APP_WAIT_ACTIVITY)
        capabilities[AppiumCapabilityType::Android::DEVICE_READY_TIMEOUT] = @environment.property(Keys::Appium::DEVICE_READY_TIMEOUT) if @environment.has_property?(Keys::Appium::DEVICE_READY_TIMEOUT)
        capabilities[AppiumCapabilityType::Android::COMPRESS_XML] = @environment.property(Keys::Appium::COMPRESS_XML) if @environment.has_property?(Keys::Appium::COMPRESS_XML)
        capabilities
      end

      def ios_capabilities
        capabilities = Selenium::WebDriver::Remote::Capabilities.new
        capabilities[:platform] = :mac
        server_capabilities(capabilities)
        capabilities[WebDriver::Remote::AppiumCapabilityType::Ios::CALENDAR_FORMAT] = @environment.property(Keys::Appium::CALENDAR_FORMAT) if @environment.has_property?(Keys::Appium::CALENDAR_FORMAT)
        capabilities[WebDriver::Remote::AppiumCapabilityType::Ios::DEVICE_NAME] = @environment.property(Keys::Appium::DEVICE_NAME) if @environment.has_property?(Keys::Appium::DEVICE_NAME)
        capabilities[WebDriver::Remote::AppiumCapabilityType::Ios::LANGUAGE] = @environment.property(Keys::Appium::LANGUAGE) if @environment.has_property?(Keys::Appium::LANGUAGE)
        capabilities[WebDriver::Remote::AppiumCapabilityType::Ios::LAUNCH_TIMEOUT] = @environment.property(Keys::Appium::LAUNCH_TIMEOUT) if @environment.has_property?(Keys::Appium::LAUNCH_TIMEOUT)
        capabilities[WebDriver::Remote::AppiumCapabilityType::Ios::LOCALE] = @environment.property(Keys::Appium::LOCALE) if @environment.has_property?(Keys::Appium::LOCALE)
        capabilities[WebDriver::Remote::AppiumCapabilityType::Ios::LOCATION_SERVICES_ENABLED] = @environment.property(Keys::Appium::LOCATION_SERVICES_ENABLED) if @environment.has_property?(Keys::Appium::LOCATION_SERVICES_ENABLED)
        capabilities[WebDriver::Remote::AppiumCapabilityType::Ios::AUTO_ACCEPT_ALERTS] = @environment.property(Keys::Appium::AUTO_ACCEPT_ALERTS) if @environment.has_property?(Keys::Appium::AUTO_ACCEPT_ALERTS)
        capabilities[WebDriver::Remote::AppiumCapabilityType::Ios::NATIVE_INSTRUMENTS_LIB] = @environment.property(Keys::Appium::NATIVE_INSTRUMENTS_LIB) if @environment.has_property?(Keys::Appium::NATIVE_INSTRUMENTS_LIB)
        capabilities
      end

      private

      def server_capabilities(capabilities)
        capabilities[:browser_name] = @environment.has_property?(Keys::Selenium::BROWSER_NAME) ? @environment.property(Keys::Selenium::BROWSER_NAME) : "''"
        capabilities[WebDriver::Remote::AppiumCapabilityType::DEVICE] = device
        capabilities[WebDriver::Remote::AppiumCapabilityType::APP] = app
        capabilities[WebDriver::Remote::AppiumCapabilityType::VERSION] = version if @environment.has_property?(Keys::Appium::VERSION)
        capabilities[WebDriver::Remote::AppiumCapabilityType::NEW_COMMAND_TIMEOUT] = new_command_timeout if @environment.has_property?(Keys::Appium::NEW_COMMAND_TIMEOUT)
        capabilities[WebDriver::Remote::AppiumCapabilityType::LAUNCH] = launch? if @environment.has_property?(Keys::Appium::LAUNCH)
      end

      def app
        raise(EnvironmentError, "Could not find property with key: '#{Keys::Appium::APP}' in the env.yml file.") unless @environment.has_property?(Keys::Appium::APP)
        app_path(@environment.property(Keys::Appium::APP))
      end

      def app_path(app)
        # If mobile web then use the original value for app.
        return app if app.downcase.eql?("safari") ||
            app.downcase.eql?("iwebview") ||
            app.downcase.eql?("chrome") ||
            app.downcase.eql?("chromium")
        Pathname.new(app).relative? ? File.realpath(app) : app
      end

      def version
        @environment.has_property?(Keys::Appium::VERSION)
      end

      def new_command_timeout
        @environment.property(Keys::Appium::NEW_COMMAND_TIMEOUT)
      end

      def launch?
        @environment.property(Keys::Appium::LAUNCH)
      end
    end
  end
end