module WebDriver
  module Configuration
    class WebDriverConfig < Base
      def initialize(environment)
        super
      end

      def appium_hub_url
        @environment.has_property?(Keys::Appium::APPIUM_HUB_URL) ? @environment.property(Keys::Appium::APPIUM_HUB_URL) : Constants::DEFAULT_APPIUM_HUB_URL
      end

      def chrome_driver_path
        chrome_driver_path = @environment.has_property?(Keys::Selenium::CHROME_DRIVER_PATH) ? @environment.property(Keys::Selenium::CHROME_DRIVER_PATH) : default_chrome_driver_path
        chrome_driver_path = File.realpath(chrome_driver_path) if Pathname.new(chrome_driver_path).relative?
        chrome_driver_path
      end

      def chrome_binary
        return @environment.property(Keys::Selenium::CHROME_BINARY) if @environment.has_property?(Keys::Selenium::CHROME_BINARY)
        nil
      end

      def ie_driver_path
        @environment.has_property?(Keys::Selenium::IE_DRIVER_PATH) ? @environment.property(Keys::Selenium::IE_DRIVER_PATH) : ie_driver_path
      end

      private

      def default_chrome_driver_path
        unless ENV["OS"] =~ /Windows/
          raise(EnvironmentError, <<-END_OF_ERROR)
Could not find property with key: '#{Keys::Selenium::CHROME_DRIVER_PATH}' in the env.yml file.

You must configure this property on machines that are not running Windows.
          END_OF_ERROR
        end
        File.expand_path(File.join(File.dirname(__FILE__), "../../../bin/chromedriver/chromedriver.exe"))
      end

      def default_ie_driver_path
        arch = OS.bits.eql?(32) ? "x86" : "x64"
        File.expand_path(File.join(File.dirname(__FILE__), "../../../bin/iedriver/#{arch}/IEDriverServer.exe"))
      end
    end
  end
end
