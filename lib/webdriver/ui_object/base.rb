module WebDriver
  module UIObject
    class Base
      def initialize(runtime, locator_registry, driver)
        @runtime, @locator_registry, @driver = runtime, locator_registry, driver
      end

      def accessor(element = nil)
        element.nil? ? DocumentAccessor.new(@locator_registry, @driver) :
            ElementAccessor.new(@locator_registry, @driver, element)
      end

      def synchroniser(element = nil)
        element.nil? ? DocumentSynchroniser.new(@locator_registry, @driver) :
            ElementSynchroniser.new(@locator_registry, @driver, element)
      end

      def conditions
        ElementConditions.new
      end
    end
  end
end