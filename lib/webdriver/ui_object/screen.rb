class Screen < WebDriver::UIObject::Base
  def initialize(runtime, locator_registry, driver)
    super
  end

  def action
    device = @runtime.environment.property(WebDriver::Configuration::Keys::Appium::DEVICE)
    if device.downcase.eql?(WebDriver::Configuration::Constants::Device::ANDROID) ||
        device.downcase.eql?(WebDriver::Configuration::Constants::Device::SELENDROID)
      AndroidAction.new(@driver)
    elsif device.downcase.include?(WebDriver::Configuration::Constants::Device::IPHONE) ||
        device.downcase.include?(WebDriver::Configuration::Constants::Device::IPAD)
      IosAction.new(@driver)
    else
      raise(UnsupportedConfigurationError, "The device: #{device} is not supported.\n")
    end
  end
end