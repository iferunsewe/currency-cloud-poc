class ElementConditions
  def initialize
    @conditions = []
  end

  def displayed?
    @conditions << [:test_displayed, []]
    self
  end

  def hidden?
    @conditions << [:test_hidden, []]
    self
  end

  def enabled?
    @conditions << [:test_enabled, []]
    self
  end

  def disabled?
    @conditions << [:test_disabled, []]
    self
  end

  def selected?
    @conditions << [:test_selected, []]
    self
  end

  def text_equals?(text, ignore_case = false)
    @conditions << [:test_text_equals, [text, ignore_case]]
    self
  end

  def text_contains?(text, ignore_case = false)
    @conditions << [:test_text_contains, [text, ignore_case]]
    self
  end

  def text_excludes?(text, ignore_case = false)
    @conditions << [:test_text_excludes, [text, ignore_case]]
    self
  end

  def attribute_contains?(attribute, value)
    @conditions << [:test_attribute_contains, [attribute, value]]
    self
  end

  def attribute_excludes?(attribute, value)
    @conditions << [:test_attribute_excludes, [attribute, value]]
    self
  end

  def css_property_contains?(css_property, value)
    @conditions << [:test_css_property_contains, [css_property, value]]
    self
  end

  def css_property_excludes?(css_property, value)
    @conditions << [:test_css_property_excludes, [css_property, value]]
    self
  end

  #
  # Tests whether an element matches the test conditions.
  #
  # @param [Selenium:WebDriver:Driver] driver The webdriver
  # @param [Selenium::WebDriver::Element] element An element
  # @return [Selenium::WebDriver::Element, nil] the element, or nil if the conditions were not met
  #
  def match(driver, element)
    begin
      test(driver, element)
    rescue Selenium::WebDriver::Error::WebDriverError
      nil
    end
  end

  #
  # Tests whether each element in an array of elements matches the test conditions. It deletes
  # elements that do not meet the conditions from the array.
  #
  # @param [Selenium:WebDriver:Driver] driver The webdriver
  # @param [Array<Selenium::WebDriver::Element>] elements An element array
  # @return [Array<Selenium::WebDriver::Element>, nil] an array of elements, or nil if the conditions were not met
  #
  def matches(driver, elements)
    begin
      matches = elements.reject {|element| test(driver, element).nil? }
      matches.empty? ? nil : matches
    rescue Selenium::WebDriver::Error::WebDriverError
      nil
    end
  end

  def to_s
    @conditions.to_s
  end

  private

  def test(driver, element)
    begin
      # Immediately return nil if the element could not be found. In other words, this tests
      # that the element is present.
      return nil if element.nil?
      # Iterate through the search conditions and return nil on the first
      # one that is not satisfied.
      @conditions.each { |method, args|
        test_args = args.dup
        if !self.send(method, driver, element, *test_args)
          return nil
        end
      }
      element
    rescue Selenium::WebDriver::Error::WebDriverError
      nil
    end
  end

  # Test for displayed
  def test_displayed(driver, element)
    begin
      element.displayed?
    rescue Selenium::WebDriver::Error::WebDriverError
      false
    rescue
      false
    end
  end

  # Test for hidden
  def test_hidden(driver, element)
    begin
      !element.displayed?
    rescue Selenium::WebDriver::Error::WebDriverError
      false
    rescue
      false
    end
  end

  # Test for enabled
  def test_enabled(driver, element)
    begin
      element.enabled?
    rescue Selenium::WebDriver::Error::WebDriverError
      false
    rescue
      false
    end
  end

  # Test for disabled
  def test_disabled(driver, element)
    begin
      !element.enabled?
    rescue Selenium::WebDriver::Error::WebDriverError
      false
    rescue
      false
    end
  end

  # Test for selected
  def test_selected(driver, element)
    begin
      element.selected?
    rescue Selenium::WebDriver::Error::WebDriverError
      false
    rescue
      false
    end
  end

  # Test for text equals
  def test_text_equals(driver, element, text, ignore_case)
    begin
      ignore_case \
        ? ElementUtils.element_text(driver, element).casecmp(text) == 0 \
        : ElementUtils.element_text(driver, element).eql?(text)
    rescue Selenium::WebDriver::Error::WebDriverError
      false
    rescue
      false
    end
  end

  # Test for text contains
  def test_text_contains(driver, element, text, ignore_case)
    begin
      ignore_case \
        ? ElementUtils.element_text(driver, element).downcase.include?(text.downcase) \
        : ElementUtils.element_text(driver, element).include?(text)
    rescue Selenium::WebDriver::Error::WebDriverError
      false
    rescue
      false
    end
  end

  # Test for text excludes
  def test_text_excludes(driver, element, text, ignore_case)
    begin
      ignore_case \
        ? !(ElementUtils.element_text(driver, element).downcase.include?(text.downcase)) \
        : !(ElementUtils.element_text(driver, element).include?(text))
    rescue Selenium::WebDriver::Error::WebDriverError
      false
    rescue
      false
    end
  end

  # Test for attribute contains
  def test_attribute_contains(driver, element, attribute, value)
    begin
      element.attribute(attribute).include? value.to_s
    rescue Selenium::WebDriver::Error::WebDriverError
      false
    rescue
      false
    end
  end

  # Test for attribute excludes
  def test_attribute_excludes(driver, element, attribute, value)
    begin
      !(element.attribute(attribute).include? value.to_s)
    rescue Selenium::WebDriver::Error::WebDriverError
      false
    rescue
      false
    end
  end

  # Test for css property contains
  def test_css_property_contains(driver, element, css_property, value)
    begin
      element.css_value(css_property).include? value.to_s
    rescue Selenium::WebDriver::Error::WebDriverError
      false
    end
  end

  # Test for css property excludes
  def test_css_property_excludes(driver, element, css_property, value)
    begin
      !(element.css_value(css_property).include? value.to_s)
    rescue Selenium::WebDriver::Error::WebDriverError
      false
    rescue
      false
    end
  end
end
