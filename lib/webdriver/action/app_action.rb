class AppAction
  def initialize(driver)
    @driver = driver
  end

  def tap(element)
    @driver.touch.single_tap(element).perform
  end

  def accept_alert
    @driver.switch_to.alert.accept
  end

  def send_keys(*keys)
    @driver.touch.send_keys(keys).perform
  end

  def shake
    @driver.execute_script("mobile: shake")
  end
end