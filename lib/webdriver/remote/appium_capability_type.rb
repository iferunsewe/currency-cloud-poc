module WebDriver
  module Remote
    class AppiumCapabilityType
      APP = "app"
      DEVICE = "device"
      VERSION = "version"
      NEW_COMMAND_TIMEOUT = "newCommandTimeout"
      LAUNCH = "launch"

      class Android
        APP_ACTIVITY = "app-activity"
        APP_PACKAGE = "app-package"
        APP_WAIT_ACTIVITY = "app-wait-activity"
        DEVICE_READY_TIMEOUT = "device-ready-timeout"
        COMPRESS_XML = "compressXml"
      end

      class Ios
        CALENDAR_FORMAT = "calendarFormat"
        DEVICE_NAME = "deviceName"
        LANGUAGE = "language"
        LAUNCH_TIMEOUT = "launchTimeout"
        LOCALE = "locale"
        LOCATION_SERVICES_ENABLED = "locationServicesEnabled"
        AUTO_ACCEPT_ALERTS = "autoAcceptAlerts"
        NATIVE_INSTRUMENTS_LIB = "nativeInstrumentsLib"
      end
    end
  end
end
