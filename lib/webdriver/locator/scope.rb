module Scope
  ELEMENT = "element"
  CHILDREN = "children"
  DESCENDANTS = "descendants"
end
