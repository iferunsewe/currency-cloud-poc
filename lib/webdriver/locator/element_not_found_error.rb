class ElementNotFoundError < StandardError
  def initialize(msg)
    super(msg)
  end
end