class Name
  attr_reader :value

  def initialize(value)
    @value = value
  end

  def by(scope, tag_description)
    case scope
    when Scope::ELEMENT
      return by_element(tag_description)
    else
      raise(LocatorError, "Unsupported scope: '#{scope}'")
    end
  end

  def by_element(tag_description)
    return { :name => value } if tag_description.eql?(TagDescription::UNIVERSAL)
    # If a tag description has been specified, then convert the name to a CSS
    # selector.
    { :css => tag_description + "[name=" + value + "]" }
  end
end