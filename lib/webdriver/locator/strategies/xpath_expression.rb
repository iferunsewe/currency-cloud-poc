class XPathExpression
  attr_reader :value

  def initialize(value)
    @value = value
  end

  def by(scope, tag_description)
    case scope
    when Scope::ELEMENT
      return by_element(tag_description)
    when Scope::CHILDREN
      return by_children(tag_description)
    when Scope::DESCENDANTS
      return by_descendants(tag_description)
    else
      raise(LocatorError, "Unsupported scope: '#{scope}'")
    end
  end

  private

  def by_element(tag_description)
    case tag_description
    when TagDescription::UNIVERSAL, TagDescription::INPUT, TagDescription::BUTTON,
        TagDescription::HYPERLINK, TagDescription::ORDERED_LIST, TagDescription::UNORDERED_LIST, TagDescription::LIST_ITEM, TagDescription::SPAN,
        TagDescription::DIV, TagDescription::SELECT, TagDescription::OPTION, TagDescription::IMAGE,
        TagDescription::TABLE, TagDescription::TABLE_BODY, TagDescription::TABLE_DATA_CELL, TagDescription::TABLE_ROW,
        TagDescription::TABLE_FOOTER
      # Split the locator string into a array using a slash delimiter.
      values = value.split("/")
      # Use the original locator unless the last value starts with a "@" character.
      return { :xpath => value } unless values.last.start_with?("@")
      # Modify the last value to contain the tag description.
      values[values.size - 1] = tag_description + "[" + values.last + "]"
      # Use the modified locator so that the search is restricted
      # to the specified tag description.
      { :xpath => values.join("/") }
    else
      raise(LocatorError, "Unsupported tag description: '#{tag_description}'")
    end
  end

  def by_children(tag_description)
    case tag_description
    when TagDescription::UNIVERSAL, TagDescription::INPUT, TagDescription::BUTTON,
        TagDescription::HYPERLINK, TagDescription::ORDERED_LIST, TagDescription::UNORDERED_LIST, TagDescription::LIST_ITEM, TagDescription::SPAN,
        TagDescription::DIV, TagDescription::SELECT, TagDescription::OPTION, TagDescription::IMAGE,
        TagDescription::TABLE, TagDescription::TABLE_BODY, TagDescription::TABLE_DATA_CELL, TagDescription::TABLE_ROW,
        TagDescription::TABLE_FOOTER
      { :xpath => value + "/" + tag_description }
    else
      raise(LocatorError, "Unsupported tag description: '#{tag_description}'")
    end
  end

  def by_descendants(tag_description)
    case tag_description
    when TagDescription::UNIVERSAL, TagDescription::INPUT, TagDescription::BUTTON,
        TagDescription::HYPERLINK, TagDescription::ORDERED_LIST, TagDescription::UNORDERED_LIST, TagDescription::LIST_ITEM, TagDescription::SPAN,
        TagDescription::DIV, TagDescription::SELECT, TagDescription::OPTION, TagDescription::IMAGE,
        TagDescription::TABLE, TagDescription::TABLE_BODY, TagDescription::TABLE_DATA_CELL, TagDescription::TABLE_ROW,
        TagDescription::TABLE_FOOTER
      { :xpath => value + "/" + tag_description }
    else
      raise(LocatorError, "Unsupported tag description: '#{tag_description}'")
    end
  end
end
