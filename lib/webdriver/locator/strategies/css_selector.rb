class CSSSelector
  attr_reader :value

  def initialize(value)
    @value = value
  end

  def by(scope, tag_description)
    case scope
    when Scope::ELEMENT
      return by_element(tag_description)
    when Scope::CHILDREN
      return by_children(tag_description)
    when Scope::DESCENDANTS
      return by_descendants(tag_description)
    else
      raise(LocatorError, "Unsupported scope: '#{scope}'")
    end
  end

  private

  def by_element(tag_description)
    case tag_description
    when TagDescription::UNIVERSAL, TagDescription::INPUT, TagDescription::BUTTON,
        TagDescription::HYPERLINK, TagDescription::ORDERED_LIST, TagDescription::UNORDERED_LIST, TagDescription::LIST_ITEM, TagDescription::SPAN,
        TagDescription::DIV, TagDescription::SELECT, TagDescription::OPTION, TagDescription::IMAGE,
        TagDescription::TABLE, TagDescription::TABLE_BODY, TagDescription::TABLE_DATA_CELL, TagDescription::TABLE_ROW,
        TagDescription::TABLE_FOOTER
      # Split the locator string into a array using a space delimiter.
      values = value.split(" ")
      # Use the original locator unless the last value starts with a "." character.
      return { :css => value } unless values.last.start_with?(".")
      # Prefix the last value with the tag description.
      values.last.prepend(tag_description)
      # Use the modified locator so that the search is restricted
      # to the specified tag description.
      { :css => values.join(" ") }
    else
      raise(LocatorError, "Unsupported tag description: '#{tag_description}'")
    end
  end

  def by_children(tag_description)
    case tag_description
    when TagDescription::UNIVERSAL, TagDescription::INPUT, TagDescription::BUTTON,
        TagDescription::HYPERLINK, TagDescription::ORDERED_LIST, TagDescription::UNORDERED_LIST, TagDescription::LIST_ITEM, TagDescription::SPAN,
        TagDescription::DIV, TagDescription::SELECT, TagDescription::OPTION, TagDescription::IMAGE,
        TagDescription::TABLE, TagDescription::TABLE_BODY, TagDescription::TABLE_DATA_CELL, TagDescription::TABLE_ROW,
        TagDescription::TABLE_FOOTER
      { :css => value + " > " + tag_description }
    else
      raise(LocatorError, "Unsupported tag description: '#{tag_description}'")
    end
  end

  def by_descendants(tag_description)
    case tag_description
    when TagDescription::UNIVERSAL, TagDescription::INPUT, TagDescription::BUTTON,
        TagDescription::HYPERLINK, TagDescription::ORDERED_LIST, TagDescription::UNORDERED_LIST, TagDescription::LIST_ITEM, TagDescription::SPAN,
        TagDescription::DIV, TagDescription::SELECT, TagDescription::OPTION, TagDescription::IMAGE,
        TagDescription::TABLE, TagDescription::TABLE_BODY, TagDescription::TABLE_DATA_CELL, TagDescription::TABLE_ROW,
        TagDescription::TABLE_FOOTER
      { :css => value + " " + tag_description }
    else
      raise(LocatorError, "Unsupported tag description: '#{tag_description}'")
    end
  end
end
