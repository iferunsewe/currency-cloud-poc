module WebDriver
  module Provider
    class SauceWebDriverProvider
      def initialize(runtime)
        @runtime = runtime
        @config = WebDriver::Configuration::SauceConfig.new(runtime.environment)
        @job_name = "Feature: #{runtime.feature_title}, Scenario: #{runtime.scenario_name}"
      end

      def get
        context = @config.has_device? ? Sauce::AppiumAwareContext.new(@config, @job_name) : Sauce::SeleniumAwareContext.new(@config, @job_name)
        context.driver
      end

      module Sauce
        class SeleniumAwareContext
          def initialize(config, job_name)
            @config, @job_name = config, job_name
          end

          def driver
            browser_name = @config.browser_name
            if browser_name.downcase.eql?(WebDriver::Configuration::Constants::BrowserName::CHROME)
              caps = @config.chrome_capabilities
            elsif browser_name.downcase.eql?(WebDriver::Configuration::Constants::BrowserName::FIREFOX)
              caps = @config.firefox_capabilities
            elsif browser_name.downcase.eql?(WebDriver::Configuration::Constants::BrowserName::IE)
              caps = @config.ie_capabilities
            elsif browser_name.downcase.eql?(WebDriver::Configuration::Constants::BrowserName::SAFARI)
              caps = @config.safari_capabilities
            else
              raise(UnsupportedConfigurationError, "The browser: #{browser_name} is not supported.\n")
            end
            caps[:name] = @job_name
            Selenium::WebDriver.for(:remote, :url => @config.remote_url, :desired_capabilities => caps)
          end
        end

        class AppiumAwareContext
          def initialize(config, job_name)
            @config, @job_name = config, job_name
          end

          def driver
            device = @config.device
            if device.downcase.eql?(WebDriver::Configuration::Constants::Device::ANDROID) || device.downcase.eql?(WebDriver::Configuration::Constants::Device::SELENDROID)
              caps = @config.android_capabilities
            elsif device.downcase.include?(WebDriver::Configuration::Constants::Device::IPHONE) || device.downcase.include?(WebDriver::Configuration::Constants::Device::IPAD)
              caps = @config.ios_capabilities
            else
              raise(UnsupportedConfigurationError, "The device: #{device} is not supported.\n")
            end
            caps[:name] = @job_name
            Selenium::WebDriver.for(:remote, :url => @config.remote_url, :desired_capabilities => caps)
          end
        end
      end
    end
  end
end

