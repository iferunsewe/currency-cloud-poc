module CurrencyCloud
  class Dashboard < Page
    def initialize(runtime, locator_registry, driver)
      super
      @environment = runtime.environment
      @logger = runtime.logger
    end

    def wait_until_logged_in?
      welcome_title = 'Welcome to Currency Cloud!'
      synchroniser.wait_until_element_located('dashboard-page.welcome-title', conditions.displayed?.text_contains?(welcome_title, true), :tag_description => TagDescription::BUTTON)
    end

    def proceed_to_beneficiary
      beneficiary_section = accessor.search_for_element('dashboard-page.add-beneficiary', conditions.displayed?)
      action.click(beneficiary_section)
    end

    def proceed_to_convert_currency
      convert_currency = accessor.search_for_element('sidebar.sidebar-item', conditions.displayed?.text_contains?('Convert', true))
      action.click(convert_currency)
    end
  end
end
