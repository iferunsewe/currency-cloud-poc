module CurrencyCloud
  class AddBeneficiary < Page
    def initialize(runtime, locator_registry, driver)
      super
      @environment = runtime.environment
      @logger = runtime.logger
    end

    def fill_form(user)
      nickname_field = accessor.search_for_element('add-beneficiary-page.nickname', conditions.displayed?)
      nickname_field.send_keys(user[:nickname])
      beneficiary_types = accessor.search_for_elements('add-beneficiary-page.type', conditions.displayed?)
      beneficiary_type = beneficiary_types.sample
      action.click(beneficiary_type)
      country = 'united kingdom'
      beneficiary_country_locator = 'add-beneficiary-page.beneficiary-country'
      beneficiary_country = accessor.search_for_element(beneficiary_country_locator, conditions.displayed?)
      search_dropdown(country, beneficiary_country, beneficiary_country_locator)
      bank_account_country_locator = 'add-beneficiary-page.bank-account-country'
      bank_account_country = accessor.search_for_element(bank_account_country_locator, conditions.displayed?)
      search_dropdown(country, bank_account_country, bank_account_country_locator)
      currency_field_locator = 'add-beneficiary-page.currency'
      currency_field = accessor.search_for_element(currency_field_locator, conditions.displayed?)
      currency_option = 'GBP'
      search_dropdown(currency_option, currency_field, currency_field_locator)
      bank_account_holder = accessor.search_for_element('add-beneficiary-page.bank-acccount-holder', conditions.displayed?)
      bank_account_holder.send_keys(user[:bank_account_holder])
      create_beneficiary = accessor.search_for_element('add-beneficiary-page.create-beneficiary', conditions.displayed?)
      action.click(create_beneficiary)
    end

    def search_dropdown(string, element, locator)
      synchroniser.wait_until_element_located(locator, conditions.displayed?)
      action.click(element)
      form_dropdown_search = accessor.search_for_element('form-dropdown-search', conditions.displayed?)
      form_dropdown_search.send_keys(string, :enter)
    end

    def scroll_dropdown(object)
      form_dropdown_list = accessor.search_for_element('form-dropdown-list', conditions.displayed?)
      action.focus(form_dropdown_list)
      form_dropdown_option = accessor.search_for_element('form-dropdown-option', conditions.displayed?.text_contains?(object, true))
      action.click(form_dropdown_option)
    end

    def payment_type(user)
      regular_sortcode_accountno = accessor.search_for_element('payment-type-page.regular-sortcode-accountno', conditions.displayed?)
      action.click(regular_sortcode_accountno)
      sort_code = accessor.search_for_element('payment-type-page.regular-sortcode', conditions.displayed?)
      sort_code.send_keys(user[:sort_code])
      account_no = accessor.search_for_element('payment-type-page.regular-accountno', conditions.displayed?)
      account_no.send_keys(user[:account_no])
      validate = accessor.search_for_element('payment-type-page.validate', conditions.displayed?)
      action.click(validate)
      wait_until_validated?
      action.click(validate)
    end

    def wait_until_validated?
      synchroniser.wait_until_element_located('payment-type-page.validation-alert', conditions.displayed?)
    end

    def back_to_beneficiaries
      back_to_beneficiaries = accessor.search_for_element('payment-type-page.back-to-index-beneficiaries', conditions.displayed?.enabled?)
      back_to_beneficiaries.click
      @logger.debug "Back to beneficiary index."
    end

    def wait_until_created?
      synchroniser.wait_until_element_located('payment-type-page.created-account-alert', conditions.displayed)
    end
  end
end
