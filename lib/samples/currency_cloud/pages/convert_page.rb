module CurrencyCloud
  class Convert < Page
    attr_accessor :amount_converting

    def initialize(runtime, locator_registry, driver)
      super
      @environment = runtime.environment
      @logger = runtime.logger
      @add_beneficiary = AddBeneficiary.new(@runtime, @locator_registry, @driver)
    end

    def select_selling_currency
      locator = 'convert-currency-page.selling-currency'
      selling_currency = accessor.search_for_element(locator, conditions.displayed?)
      currency = 'HKD'
      @add_beneficiary.search_dropdown(currency, selling_currency, locator)
    end

    def select_buying_currency
      locator = 'convert-currency-page.buying-currency'
      buying_currency = accessor.search_for_element(locator, conditions.displayed?)
      currency = 'GBP'
      @add_beneficiary.search_dropdown(currency, buying_currency, locator)
    end

    def select_convert_option(option)
      conversion_options = accessor.search_for_element('convert-currency-page.conversion-options', conditions.displayed?)
      action.click(conversion_options)
      conversion_option = accessor.search_for_element('convert-currency-page.conversion-option', conditions.displayed?.text_contains?(option, true))
      action.click(conversion_option)
    end

    def select_amount_to_convert
      conversion_amount = accessor.search_for_element('convert-currency-page.conversion-amount', conditions.displayed?)
      4.times{conversion_amount.send_keys :backspace}
      @amount_converting = 100
      conversion_amount.send_keys(amount_converting)
    end

    def select_conversion_date
      synchroniser.wait_until_element_located('convert-currency-page.conversion-calendar', conditions.displayed?, :timeout => 25)
      conversion_calendar = accessor.search_for_element('convert-currency-page.conversion-calendar', conditions.displayed?)
      conversion_calendar.click
      calendar_dates = accessor.search_for_elements('convert-currency-page.conversion-dates', conditions.displayed?)
      calendar_date = calendar_dates.sample
      calendar_date.click
    end

    def get_quote
      get_quote = accessor.search_for_element('convert-currency-page.get-a-quote', conditions.displayed?)
      action.click(get_quote)
    end

    def check_calculated_buying_amount
      synchroniser.wait_until_element_located('convert-currency-page.quote-title', conditions.displayed?, :timeout => 25)
      exchange_rate = (accessor.store_element_text('convert-currency-page.exchange-rate', conditions.displayed?)).to_f
      buying_amount = (accessor.store_element_text('convert-currency-page.buying-amount', conditions.displayed?)).to_f
      selling_amount = (accessor.store_element_text('convert-currency-page.selling-amount', conditions.displayed?)).to_f
      if plus_or_minus(@amount_converting) === buying_amount * exchange_rate
        true
      elsif plus_or_minus(@amount_converting) === selling_amount * exchange_rate
        true
      else
        false
      end
    end

  private

    def plus_or_minus(num)
      (num..num+0.5) || (num..num-0.5)
    end

  end
end