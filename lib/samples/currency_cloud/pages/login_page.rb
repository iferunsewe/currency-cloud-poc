module CurrencyCloud
  class Login < Page
    def initialize(runtime, locator_registry, driver)
      super
      @environment = runtime.environment
      @logger = runtime.logger
    end

    def open
      action.navigate_to(@environment.property('currency-cloud-url'))
    end

    def wait_until_opened?
      synchroniser.wait_until_element_located('login-page.username-label', conditions.displayed?, :tag_description => TagDescription::BUTTON)
    end

    def login(user)
      id_field = accessor.search_for_element('login-page.username-field', conditions.displayed?)
      id_field.send_keys(user[:email])
      go_button = accessor.search_for_element('login-page.go-button', conditions.displayed?)
      action.click(go_button)
      password_field = accessor.search_for_element('login-page.password-field', conditions.displayed?)
      password_field.send_keys(user[:password])
      favourite_field = accessor.search_for_element('login-page.secret-question-field', conditions.displayed?)
      favourite_field.send_keys(user[:favourite])
      action.click(go_button)
    end

    def wait_until_alert_located?
      alert_msg_login_unsuccessful = "Login Unsuccessful"
      synchroniser.wait_until_element_located('login-page.alert-login-unsuccessful', conditions.displayed?.text_equals?(alert_msg_login_unsuccessful, true), :tag_description => TagDescription::BUTTON)
    end

    def close_window
      action.close_window
    end

    def wait_until_validation_located?
      synchroniser.wait_until_element_located('login-page.alert-blank-field', conditions.displayed?, :tag_description => TagDescription::BUTTON)
    end


  end
end
