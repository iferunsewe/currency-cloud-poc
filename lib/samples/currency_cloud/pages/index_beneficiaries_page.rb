module CurrencyCloud
  class IndexBeneficiaries < Page
    def initialize(runtime, locator_registry, driver)
      super
      @environment = runtime.environment
      @logger = runtime.logger
    end

    def wait_until_user_is_located?(user)
      synchroniser.wait_until_element_located('beneficiaries-index-page.table-rows', conditions.displayed?)
      beneficiaries =  accessor.store_elements_text('beneficiaries-index-page.table-row.nickname', conditions.displayed?.enabled?)
      p beneficiaries
      beneficiaries.include?(user[:nickname])
    end
  end
end