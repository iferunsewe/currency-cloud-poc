module TestApp
  class HomeScreen < Screen
    def initialize(runtime, locator_registry, driver)
      super
    end

    def enter_text_field1(text)
      text_field1 = accessor.search_for_element("testapp.textfield1", conditions.displayed?)
      action.tap(text_field1)
      text_field1.send_keys(text)
    end

    def enter_text_field2(text)
      text_field1 = accessor.search_for_element("testapp.textfield2", conditions.displayed?)
      action.tap(text_field1)
      text_field1.send_keys(text)
    end
  end
end