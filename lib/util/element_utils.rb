module ElementUtils
  def self.element_text(driver, element)
    begin
      return element.attribute("value") if element.tag_name.downcase == "input"
      element.displayed? ? element.text : driver.execute_script("if (typeof jQuery === 'undefined') { return arguments[0].innerText; } else { return $(arguments[0]).text(); }", element)
    rescue Selenium::WebDriver::Error::WebDriverError
      nil
    end
  end
end
