module StringUtils
  def self.squish(text)
    text.strip.gsub(/\s+/, " ")
  end
end
