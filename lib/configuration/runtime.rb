require 'log4r'
require 'log4r/yamlconfigurator'

class Runtime
  include Log4r

  attr_accessor :environment, :feature_title, :scenario_name

  def initialize(scenario, environment)
    case scenario
    when Cucumber::Ast::Scenario
      @feature_title = scenario.feature.title
      @scenario_name = scenario.name
    when Cucumber::Ast::OutlineTable::ExampleRow
      @feature_title = scenario.scenario_outline.feature.title
      @scenario_name = scenario.scenario_outline.name
    end
    @environment = environment
    Log4r::YamlConfigurator.decode_yaml(log4r_yml)
  end

  def logger(profile = environment.property("log4r-profile"))
    Log4r::Logger[profile]
  end

  private

  def log4r_yml_defined?
    log4r_file && File.exist?(log4r_file)
  end

  def log4r_yml
    unless log4r_yml_defined?
      raise(ProfilesNotDefinedError, "log4r.yml was not found.  Current directory is #{Dir.pwd}.\n")
    end

    begin
      @log4r_yml = YAML.load_file(log4r_file)
    rescue StandardError => e
      raise(YmlLoadError, "log4r.yml was found, but could not be parsed.\n")
    end

    if @log4r_yml.nil? || !@log4r_yml.is_a?(Hash)
      raise(YmlLoadError, "log4r.yml was found, but was blank or malformed.\n")
    end

    @log4r_yml
  end

  # Locates log4r.yml file. The file can end in .yml or .yaml,
  # and be located in the current directory (eg. project root) or
  # in a .config/ or config/ subdirectory of the current directory.
  def log4r_file
    @log4r_file ||= Dir.glob('{,.config/,config/}log4r{.yml,.yaml}').first
  end
end