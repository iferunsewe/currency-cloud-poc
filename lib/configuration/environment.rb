class Environment
  def initialize
    profile_key = ENV['env'] || "default"
    unless env_yml.has_key?(profile_key)
      raise(KeyError, <<-END_OF_ERROR)
Could not find profile key: '#{profile_key}'

Defined profile keys in env.yml:
  * #{env_yml.keys.sort.join("\n  * ")}
      END_OF_ERROR
    end
    @data = env_yml[profile_key]
  end

  def has_property?(property_key)
    @data.has_key?(property_key)
  end

  def property(property_key)
    unless has_property?(property_key)
      raise(KeyError, <<-END_OF_ERROR)
Could not find property key: '#{property_key}'

Defined property keys in env.yml:
  * #{@data.keys.sort.join("\n  * ")}
      END_OF_ERROR
    end
    @data[property_key]
  end

  private

  def env_yml_defined?
    env_file && File.exist?(env_file)
  end

  def env_yml
    unless env_yml_defined?
      raise(ProfilesNotDefinedError,"env.yml was not found.  Current directory is #{Dir.pwd}. You must define a 'default' profile for the environment.\n")
    end

    begin
      @env_yml = YAML.load_file(env_file)
    rescue StandardError => e
      raise(YmlLoadError,"env.yml was found, but could not be parsed.\n")
    end

    if @env_yml.nil? || !@env_yml.is_a?(Hash)
      raise(YmlLoadError,"env.yml was found, but was blank or malformed.\n")
    end

    @env_yml
  end

  # Locates env.yml file. The file can end in .yml or .yaml,
  # and be located in the current directory (eg. project root) or
  # in a .config/ or config/ subdirectory of the current directory.
  def env_file
    @env_file ||= Dir.glob('{,.config/,config/}env{.yml,.yaml}').first
  end
end
