﻿MagenTys Ruby Automation Framework
==================================

This document serves as a quick guide for setting up a new client-specific instance of our ruby test automation framework.

Pre-requisites
--------------

To develop the automated tests you will need to first download the following software, if it's not already installed:

* __Ruby (Version: 1.9.3)__ and the right __DevKit__ for the version of ruby - if it's not installed, get it from the RubyInstaller website:  
  [http://rubyinstaller.org/downloads/](http://rubyinstaller.org/downloads/)

* __Cygwin (Version: Latest)__ - if it's not installed, get it from the Cygwin website:  
  [http://www.cygwin.com/](http://www.cygwin.com/)

Installation
------------

The following software must be installed in the specified order:

	1. Ruby
	2. DevKit
	3. Bundler Gem

The following software can optionally be installed in no specific order:

	1. Cygwin

###Ruby

1.	Launch the RubyInstaller from the website and follow instructions. When prompted, choose to add ruby executables to your path
2.	Finally, in `cmd.exe` console type:  `ruby --version`, to determine if the installation has been successful

###DevKit

1. First create a folder __devkit__ under __<RUBY\_INSTALL\_DIR>__
2. Launch the DevKit extractor, e.g. `DevKit-tdm-32-4.5.2-20111229-1559-sfx.exe` and choose to extract the files into the __devkit__ folder
3. In `cmd.exe` console type: `cd <RUBY_INSTALL_DIR>\devkit`
3. In `cmd.exe` console type: `ruby dk.rb init`, to create the configuration file config.yml
4. Finally, in `cmd.exe` console type: `ruby dk.rb install`, to DevKit enhance your installed Rubies.  
   This step installs (or updates) an operating\_system.rb file into the relevant directory needed to implement a RubyGems pre\_install hook and a devkit.rb helper library file into __<RUBY\_INSTALL\_DIR>\lib\ruby\site\ruby__.  

###Bundler Gem   

1. In `cmd.exe` console type: `gem install bundler`

###Cygwin

1.	Launch `setup.exe` and follow instructions
2.	Select the default settings for all steps
3.	On installation completion, launch cygwin from the shortcut on the desktop
4.	Immediately close cygwin, as the previous step should have setup a user home directory. This should be located under __C:\cygwin\home\<username>__
5.	Finally, copy __.minttyrc__ to __C:\cygwin\home\<username>__

Configuration
-------------

In config folder there are the following files:

__cucumber.yml__ - this contains the cucumber settings  
__env.yml__ - this contains environment settings  
__locator.yml__ - used with selenium this contains the locators finding elements, i.e. CSS selectors, XPath expressions  

If using selenium webdriver with the chrome browser, ensure you have the chrome browser and chromedriver setup   

Gem dependencies
----------------

Installing gem dependencies only needs to be run for first time setup, or if the Gemfile is modified.  

In `cmd.exe` console type:  

1. `bundle update`
2. `bundle install`

Running the tests
-----------------

In `cmd.exe` console type either:  

* `bundle exec cucumber`  
  _this will run the scenarios using the default environment._  
* or, `bundle exec cucumber env=test`  
  _this will run the scenarios using the __test__ environment._  

Running the tests in parallel
-----------------------------

In `cmd.exe` console type either:  

* `bundle exec rake parallel_cucumber`  
  _this will run the features in parallel using the default environment._  
* or, `bundle exec rake parallel_cucumber env=test`  
  _this will run the features in parallel using the __test__ environment._  
* or, `bundle exec rake parallel_cucumber env=test profile=grid`  
  _this will run the features in parallel using the __test__ environment and __grid__ profile._  
* or, `bundle exec rake parallel_cucumber env=test threads=4`  
  _this will run the features in parallel in __4__ threads using the __test__ environment._  

If any of the scenarios fail during a parallel run you will see:  

__rake aborted!__
__Command failed with status (1): [bundle exec parallel_cucumber features/...]__
