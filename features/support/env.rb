# Rubygems and Bundler
require "rubygems"
require "bundler/setup"

# Gems
require "selenium-webdriver"
require "rspec/expectations"
require "os"
require "pathname"
require "cucumber"
require "xmlsimple"
require "net/http"
require "nokogiri"
require "active_support/all"
require "require_all"

require_all 'lib'

shared_driver = nil
environment = Environment.new
locator_registry = environment.has_property?(WebDriver::Configuration::Keys::LOCATOR_PROFILE) ?
    LocatorRegistry.new.use_profile(environment.property(WebDriver::Configuration::Keys::LOCATOR_PROFILE)) :
    LocatorRegistry.new

# Only run the clean screenshots once.
Before do
  $clean_screenshots ||= false
  unless $clean_screenshots
    Dir.mkdir("screenshots") unless File.exists?("screenshots")
    Dir.glob("screenshots/*").each { |filename| File.delete(filename) }
    $clean_screenshots = true
  end
end

Before do |scenario|
  @runtime = Runtime.new(scenario, environment)
  @locator_registry = locator_registry
  shared_driver = WebDriver::Provider::WebDriverProvider.new(@runtime).get if shared_driver.nil?
  @driver = shared_driver
end

#
# After hooks run in the opposite order of which they are registered.
#

# If you want to take a screenshot before running all other After hooks,
# then move this to a hooks.rb file and register it last.
After do |scenario|
  if scenario.failed?
    @driver.save_screenshot("screenshots/#{scenario.__id__}.png")
    embed("screenshots/#{scenario.__id__}.png", "image/png", "Screenshot")
  end
end

at_exit do
  shared_driver.quit unless shared_driver.nil?
end
