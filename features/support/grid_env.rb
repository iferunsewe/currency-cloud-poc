# Rubygems and Bundler
require "rubygems"
require "bundler/setup"

# Gems
require "selenium-webdriver"
require "rspec/expectations"
require "os"
require "pathname"
require "cucumber"
require "xmlsimple"
require "net/http"
require "nokogiri"
require "active_support/all"
require "require_all"

require_all 'lib'

environment = Environment.new
locator_registry = environment.has_property?(WebDriver::Configuration::Keys::LOCATOR_PROFILE) ?
    LocatorRegistry.new.use_profile(environment.property(WebDriver::Configuration::Keys::LOCATOR_PROFILE)) :
    LocatorRegistry.new

# Only run the clean screenshots once.
Before do
  $clean_screenshots ||= false
  unless $clean_screenshots
    Dir.mkdir("screenshots") unless File.exists?("screenshots")
    Dir.glob("screenshots/*").each { |filename| File.delete(filename) }
    $clean_screenshots = true
  end
end

Before do |scenario|
  @runtime = Runtime.new(scenario, environment)
  @locator_registry = locator_registry
  @driver = WebDriver::Provider::RemoteWebDriverProvider.new(@runtime).get
end

#
# After hooks run in the opposite order of which they are registered.
#

After do
  @driver.quit unless @driver.nil?
end

# If you want to take a screenshot before running all other After hooks,
# then move this to a hooks.rb file and register it last.
After do |scenario|
  if scenario.failed?
    @driver.save_screenshot("screenshots/#{scenario.__id__}.jpg")
    embed("screenshots/#{scenario.__id__}.jpg", "image/jpg", "Screenshot")
  end
end
