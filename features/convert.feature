Feature: Convert currency
  As a user
  I want to convert a currency
  So I can pay a beneficiary

  @wip
  Scenario: Users can convert currency
    Given I have selected the currencies I want to use
    When I specify an amount to sell
    And I select a conversion date
    And I request a quote
    Then I should see a quote with the calculated buying amount
