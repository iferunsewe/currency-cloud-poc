Feature: Add beneficiary
  As a user
  I want to be able to add a beneficiary
  So I can pay them

  @complete
  Scenario: User adds beneficiary
    Given I have logged in
    When I add a beneficiary
    Then the beneficiary should appear on the index page