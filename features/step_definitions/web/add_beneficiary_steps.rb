Given /^I have logged in$/ do
  # @login_page.open
  # phil = {email: 'philip.young@magentys.io', password: 'magentys', favourite: 'magentys'}
  # @user.login(phil)
  # expect(@login_page.wait_until_opened?).to be true
  steps %Q{
    Given I am on the login page
    When I login
    Then I should be directed to the user's dashboard
  }
end

When /^I add a beneficiary$/ do
  @dashboard.proceed_to_beneficiary
  @user = {nickname: 'vputin', bank_account_holder: 'Vladmir Putin', sort_code: "202464", account_no: "20245941"}
  @add_beneficiary.fill_form(@user)
  @add_beneficiary.payment_type(@user)
end

Then /^the beneficiary should appear on the index page$/ do
  @add_beneficiary.back_to_beneficiaries
  expect(@index_beneficiaries.wait_until_user_is_located?(@user)).to be true
end