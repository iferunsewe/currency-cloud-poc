Given /^I am on the login page$/ do
  @login_page.open
  expect(@login_page.wait_until_opened?).to be true
end

When /^I login$/ do
  phil = {email: 'philip.young@magentys.io', password: 'magentys', favourite: 'magentys'}
  @user.login(phil)
end

Then /^I should be directed to the user's dashboard$/ do
  expect(@dashboard.wait_until_logged_in?).to be true
end

When /^I enter in the wrong information$/ do
  @login_page.open
  phil = {email: 'philip.young@magentys.io', password: 'magentyss', favourite: 'magentys'}
  @user.login(phil)
end

Then /^the login should be unsuccessful$/ do
  expect(@login_page.wait_until_alert_located?).to be true
end

When /^I leave a field blank$/ do
  @login_page.open
  phil = {email: 'philip.young@magentys.io', password: 'magentys', favourite: ''}
  @user.login(phil)
end

Then /^a validation message should be displayed$/ do
  expect(@login_page.wait_until_validation_located?).to be true
end

