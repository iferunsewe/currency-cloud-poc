Before do
  @login_page ||= CurrencyCloud::Login.new(@runtime, @locator_registry, @driver)
  @user ||= CurrencyCloud::Login.new(@runtime, @locator_registry, @driver)
  @dashboard ||= CurrencyCloud::Dashboard.new(@runtime, @locator_registry, @driver)
  @add_beneficiary ||= CurrencyCloud::AddBeneficiary.new(@runtime, @locator_registry, @driver)
  @index_beneficiaries ||= CurrencyCloud::IndexBeneficiaries.new(@runtime, @locator_registry, @driver)
  @convert ||= CurrencyCloud::Convert.new(@runtime, @locator_registry, @driver)
end