Given (/^I have selected the currencies I want to use$/) do
  steps %Q{
    Given I am on the login page
    When I login
    Then I should be directed to the user's dashboard
  }
  @dashboard.proceed_to_convert_currency
  @convert.select_selling_currency
  @convert.select_buying_currency
end


When (/^I specify an amount to (sell|buy)$/) do |option|
  @convert.select_convert_option(option)
  @convert.select_amount_to_convert
end

And (/^I select a conversion date$/) do
  @convert.select_conversion_date
end

And (/^I request a quote$/) do
  @convert.get_quote
end

Then (/^I should see a quote with the calculated buying|selling amount$/) do
  expect(@convert.check_calculated_buying_amount).to be true
end