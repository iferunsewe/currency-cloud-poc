Feature: Login
  As a user
  I want to be able to login
  So I can use the website

  @complete
  Scenario: Users can login
    Given I am on the login page
    When I login
    Then I should be directed to the user's dashboard

  @complete
  Scenario: Users enter wrong information to a field
    When I enter in the wrong information
    Then the login should be unsuccessful

  @complete
  Scenario: Users leave field blank
    When I leave a field blank
    Then a validation message should be displayed